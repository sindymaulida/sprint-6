<?php

class Dosen_model {
       private $table = 'Dosen';
       private $db;

       public function __construct()
       {
           $this->db = new Database;
       }

        public function getAllDosen()
        {
           $this->db->query('SELECT * FROM ' . $this->table);
           return $this->db->resultSet();
        }

        public function getDosenById($id)
        {
            $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id');
            $this->db->bind('id', $id);
            return $this->db->single();
        }

        public function tambahDataDosen($data)
        {
            $query = "INSERT INTO Dosen
                        VALUES
                       (NULL, :nama, :telp, :email, :mata_kuliah, :alamat)";
            
            $this->db->query($query); 
            $this->db->bind('nama', $data['nama']); 
            $this->db->bind('telp', $data['telp']);  
            $this->db->bind('email', $data['email']); 
            $this->db->bind('mata_kuliah', $data['mata_kuliah']); 
            $this->db->bind('alamat', $data['alamat']); 

            $this->db->execute();
           return $this->db->rowCount();

        }

        public function hapusDataDosen($id)
        {
            $query = "DELETE FROM Dosen WHERE id = :id";
            $this->db->query($query);
            $this->db->bind('id', $id);

            $this->db->execute();

            return $this->db->rowCount();
        }


        public function ubahDataDosen($data)
        {
            $query = "UPDATE Dosen SET
                        nama = :nama,
                        telp = :telp,
                        email = :email,
                        mata_kuliah = :mata_kuliah,
                        alamat = :alamat
                        WHERE id = :id";
            
            $this->db->query($query); 
            $this->db->bind('nama', $data['nama']); 
            $this->db->bind('telp', $data['telp']);  
            $this->db->bind('email', $data['email']); 
            $this->db->bind('mata_kuliah', $data['mata_kuliah']); 
            $this->db->bind('alamat', $data['alamat']); 
            $this->db->bind('id', $data['id']);
            $this->db->execute();
            // var_dump($data['id']);
            // return var_dump($this->db->execute());
            return $this->db->rowCount();

        }

        public function cariDataDosen()
        {
            $keyword = $_POST['keyword'];
            $query = "SELECT * FROM Dosen WHERE nama LIKE %keyword%";
            $this->db->query($query);
            $this->db->bind('keyword', "%keyword%");
            return $this->db->resultSet();
        }
}