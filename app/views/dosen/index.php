<div class="container mt-3">

     <div class="row">
        <div class Flasher::flash(); ?>
        </div>
      </div> 


    <div class="row">
        <div class="col-6">
        <button type="button" class="btn btn-primary" data-toggle="modal" 
        data-target="#exampleModal">
            Tambah Data Dosen
        </button>


        <div class="row mt-4">
        <div class="col-12">
          <from action="<?= BASEURL; ?>/Dosen/cari" method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="cari Dosen..." name="keyword" id="keyword" autocomplete="off">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit" id="tombolCari">Cari</button>
            </div>
        </div>
          </form>
      </div>
    </div> 
       
            <h3>Daftar Dosen</h3>
                <ul class="list-group">
                    <?php foreach( $data['mhs'] as $mhs ) : ?>
                    <li class="list-group-item">
                        <?= $mhs['nama']; ?>
                        <a href="<?= BASEURL; ?>/Dosen/hapus/<?= $mhs['id']; ?>" 
                        class="badge badge-danger float-right ml-1"
                          onclick="return confirm('yakin?');">Hapus</a>
                          <a href="<?= BASEURL; ?>/Dosen/tampilData/<?= $mhs['id']; ?>" 
                        class="badge badge-success float-right">Ubah</a>
                        <a href="<?= BASEURL; ?>/Dosen/detail/<?= $mhs['id']; ?>" 
                        class="badge badge-primary float-right ml-1">Detail</a>
                    </li>                    
                    <?php endforeach; ?> 
                </ul>
        </div>
    </div>
   

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="judulModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="<?= BASEURL; ?>/Dosen/tambah" method="post">
            <input type="hidden" nama="id" id="id">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama">
            </div>

            <div class="form-group">
                <label for="telp">Telp</label>
                <input type="number" class="form-control" id="telp" name="telp"> 
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="mata_kuliah">Mata_kuliah</label>
                <select class="form-control" id="mata_kuliah" name="mata_kuliah">
                  <option>Teknik Informatika</option>
                  <option>Teknik Komputer & Jaringan</option>
                  <option>Manajemen</option>
                  <option>Akutansi</option>
                  <option>TKPI</option>
                </select>
            </div>

            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat">
            </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>

