<?php

class Dosen extends Controller {
    public function index()
    {
        $data['judul'] = 'Daftar Dosen';
        $data['mhs'] = $this->model('Dosen_model')->getAllDosen();
        $this->view('templates/header', $data);
        $this->view('dosen/index', $data);
        $this->view('templates/footer');
    }

    public function detail($id)
    {
        $data['judul'] = 'Detail Dosen';
        $data['mhs'] = $this->model('Dosen_model')->getDosenById($id);
        $this->view('templates/header', $data);
        $this->view('dosen/detail', $data);
        $this->view('templates/footer');
    }

    public function tambah()
    {
        if($this->model('Dosen_model')->tambahDataDosen($_POST) > 0) { // method tambahDataDosennnn mana
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . BASEURL . '/Dosen');
            exit;
        }else {
            Flasher::setFlash('gagal', 'ditambahkan', 'denger');
            header('Location: ' . BASEURL . '/Dosen');
            exit;
        }
    }


     public function hapus($id)
     {           
         if($this->model('Dosen_model')->hapusDataDosen($id) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            header('Location: ' . BASEURL . '/Dosen');
            exit;
       }else {
            Flasher::setFlash('gagal', 'dihapus', 'denger');
           header('Location: ' . BASEURL . '/Dosen');
            exit;
        }
    }


    // public function getUbah()//ngirim data melalui json/ajax
    // {
    //     echo json_encode($this->model('Dosen_model')->getDosenById($_POST['id'])); //yg ini udah sama? sudah
    // }

    // ica lihat juga gk?... ica?

    public function tampilData($id)//jadi disini kita ngambil data /id nah kita tampilin fdi form edit
    {
        $data['judul'] = 'Update Dosen';
        $data['dosen'] = $this->model('Dosen_model')->getDosenById($id);
        // var_dump($data['dosen']['id']);
        $this->view('templates/header', $data);
        $this->view('dosen/update', $data);
        $this->view('templates/footer');
    }

    public function ubah()
    {
        // var_dump($this->model('Dosen_model')->ubahDataDosen($_POST) > 0);
        //pesannya gk keluar, pas kita udah selesai, berhasil apa gagal
        //ini ada perintah exit gk punya ica,ada
        //ica klw nambah data flashernya kluarkan?keluar,tolong disamainin codingan iwa, ,udah aku samain dan udah aku cek berulang kali juga
        //gimana,,mau diskip? ya shraunya ini bisa...mnurut ane.. cobe ce bagian if ini
        if($this->model('Dosen_model')->ubahDataDosen($_POST) > 0) { 
            Flasher::setFlash('berhasil', 'diubah', 'success');
            header('Location: ' . BASEURL . '/Dosen');//coba lihat punya ica kyk gini juga gk headernya, ada return gk dibawah elsenya sama kaya gitu juga
            exit;
        }else {
            Flasher::setFlash('gagal', 'diubah', 'denger');
            header('Location: ' . BASEURL . '/Dosen');
        } 
    }

    public function cari()
    {
        $data['judul'] = 'Daftar Dosen';
        $data['mhs'] = $this->model('Dosen_model')->cariDataDosen();
        $this->view('templates/header', $data);
        $this->view('dosen/index', $data);
        $this->view('templates/footer');
    }

}


